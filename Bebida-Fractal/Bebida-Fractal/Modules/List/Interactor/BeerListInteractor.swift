//
//  BeerListInteractor.swift
//  Bebida-Fractal
//
//  Created by Fernanda de Lima on 13/12/2017.
//  Copyright © 2017 Empresinha. All rights reserved.
//

import Foundation

class BeerListInteractor: BeerListInteractorInputProtocol{
    weak var presenter: BeerListInteractorOutputProtocol?
    var localDatamanager: BeerListLocalDataManagerInputProtocol?
    var remoteDatamanager: BeerListRemoteDataManagerInputProtocol?
    
    func retrieveBeerList() {
        do {
            remoteDatamanager?.retrieveBeerList()
            
        } catch {
            presenter?.didRetrieveBeers([])
        }
    }
    
}

extension BeerListInteractor: BeerListRemoteDataManagerOutputProtocol {
    
    func onBeersRetrieved(_ beers: [Beer]) {
        presenter?.didRetrieveBeers(beers)
    }
    
    func onError() {
        presenter?.onError()
    }
    
}

