//
//  BeerViewCell.swift
//  Bebida-Fractal
//
//  Created by Fernanda de Lima on 13/12/2017.
//  Copyright © 2017 Empresinha. All rights reserved.
//

import UIKit
import AlamofireImage

class BeerViewCell: UITableViewCell {

    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tagLineLabel: UILabel!
    @IBOutlet weak var favoriteImage: UIImageView!
    
    func set(forBeer beer: Beer) {
        self.selectionStyle = .none
        titleLabel?.text = beer.title
        tagLineLabel.text = beer.tagLine
        let url = URL(string: beer.imageUrl)!
        let placeholderImage = UIImage(named: "placeholder")!
//        let filter = AspectScaledToFillSizeFilter(size: postImageView.frame.size)
        postImageView?.af_setImage(withURL: url, placeholderImage: placeholderImage)
    }
}
